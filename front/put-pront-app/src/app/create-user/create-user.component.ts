import { Component, OnInit } from '@angular/core';
import { User } from '../user';

import { ApiService } from '../services/api.service';

import { NgForm, FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.css']
})
export class CreateUserComponent implements OnInit {

  model = new User('', '', '');

  // constructor(private api: ApiService) { }

  form: FormGroup;

  constructor(private api: ApiService) {
  }

  ngOnInit() {}

  onSubmit(form: NgForm) {
    console.log(form);
    this.api.createUser(this.model.email, this.model.name, this.model.password);
  }
}
