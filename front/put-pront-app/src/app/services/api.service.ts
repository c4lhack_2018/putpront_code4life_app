import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {
  LoginUserRequest,
  LoginUserResponse,
  CreateUserRequest,
  CreateEvetRequest,
  Goal
} from '../apiData';
import { Observable } from 'rxjs';
import { Ceremony } from '../ceremony';
import { Donate } from '../donate';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  static url = 'http://autismservice.azurewebsites.net/api/';

  constructor(private http: HttpClient) {}

  getEventByPath(path: string) {
    return this.http.get(
      ApiService.url + 'GetEventData/' + path
    );
  }

  getMotives() {
    return this.http.get(ApiService.url + 'GetCertificates/');
  }

  getAllUserEvents(id: number) {
    return this.http.get(ApiService.url + 'GetAllEvents/${id}');
  }

  public loginUser(
    email: string,
    password: string
  ): Observable<LoginUserResponse> {
    const request = <LoginUserRequest>{
      email: email,
      password: password
    };

    return this.http.post<LoginUserResponse>(
      ApiService.url + 'LoginUser',
      request
    );
  }

  public createUser(email: string, name: string, password: string): void {
    const request = <CreateUserRequest>{
      email: email,
      login: name,
      password: password
    };

    this.http
      .post(ApiService.url + 'CreateUser', request)
      .subscribe(r => console.log(r));
  }

  public uploadPhoto(fileToUpload: any, uploadUrl) {
    const input = new FormData();
    input.append('file', fileToUpload);

    return this.http.post(uploadUrl, input);
  }

  public createEvent(event: Ceremony): void {
    const request = <CreateEvetRequest>{
      accountId: event.accountId,
      certificate: event.certificate,
      date: new Date(event.date).toISOString(),
      donationAmount: event.donationAmmount,
      donationSum: event.donationSum,
      goalId: event.goalId,
      link: event.link,
      picUrl: event.picUrl,
      text: event.text,
      title: event.title
    };

    this.http
      .post(ApiService.url + 'CreateEvent', request)
      .subscribe(r => console.log(r));
  }

  public getGoals(): Observable<Goal[]> {
    return this.http.get<Goal[]>(ApiService.url + 'getGoals/getGoals');
  }

  public makePayment(): Observable<any> {
    return this.http.get<Goal[]>(ApiService.url + 'WaitForMoney');
  }

  public makeDonation(doantion: Donate) {
    console.log(doantion);
    doantion.MotivesId = 1;
    return this.http.post(ApiService.url + 'CreateEvent', doantion);
  }
}
