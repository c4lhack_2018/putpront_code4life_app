import { Component, OnInit, ElementRef } from '@angular/core';
import { Ceremony } from '../ceremony';

import { ApiService } from '../services/api.service';
import { Goal } from '../apiData';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { LoginUtils } from '../login-utils';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-event',
  templateUrl: './create-event.component.html',
  styleUrls: ['./create-event.component.css']
})
export class CreateEventComponent implements OnInit {
  model = new Ceremony('', new Date(), '', '', true, 0, 0, 0, '', 1);
  public goals: Goal[];

  @ViewChild('fileInput') fileInput: ElementRef;
  @ViewChild('addEventPhotoFormElement') addEventPhotoFormVC: ElementRef;
  addEventPhotoForm: HTMLFormElement;

  @ViewChild('createEventFormElement')
  createEventFormVC: ElementRef;
  createEventForm: HTMLFormElement;

  @ViewChild('steps')
  steps: ElementRef;

  @ViewChild('loading')
  loading: ElementRef;

  @ViewChild('seeEvent')
  seeEvent: ElementRef;

  constructor(private api: ApiService, private router: Router) {}

  ngOnInit() {
    LoginUtils.proceedIfLogged(
      () => {
        this.addEventPhotoForm = <HTMLFormElement>(
          this.addEventPhotoFormVC.nativeElement
        );
        this.addEventPhotoForm.style.display = 'none';

        this.createEventForm = <HTMLFormElement>(
          this.createEventFormVC.nativeElement
        );

        this.api.getGoals().subscribe(res => {
          this.goals = res;
          console.log(this.goals);
        });
      },
      this.router
    );
  }

  onFormSubmit(): void {
    // go to next step
    this.steps.nativeElement.children[0].classList.remove('active');
    this.steps.nativeElement.children[1].classList.add('active');
    console.log(
      this.steps.nativeElement.children[0].classList.remove('active')
    );
    this.createEventForm.style.display = 'none';
    this.addEventPhotoForm.style.display = 'block';
  }

  onDocumentSubmit(): void {
    const fi = this.fileInput.nativeElement;

    if (fi.files && fi.files[0]) {
      const fileToUpload = fi.files[0];

      // hide button
      document.getElementById('buttonSubmit').classList.add('not-visible');

      // show loading
      this.loading.nativeElement.classList.add('visible');

      this.api
        .uploadPhoto(
          fileToUpload,
          'http://autismservice.azurewebsites.net/api/UploadImage'
        ) // 5000 for macc
        .subscribe(response => {
          this.sendData(response);
          this.loading.nativeElement.classList.add('loaded');
          this.seeEvent.nativeElement.classList.add('loaded');
        });
    }
  }

  sendData(response: any): void {
    // this.model.date = this.model.date.toISOString();
    console.log('data send');
    this.model.picUrl = response[0].url;
    console.log(this.model);
    this.api.createEvent(this.model);
  }
}
