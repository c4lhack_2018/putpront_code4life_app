import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SignFormComponent } from './sign-form/sign-form.component';
import { HomepageComponent } from './homepage/homepage.component';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { LoginComponent } from './login/login.component';
import { CreateUserComponent } from './create-user/create-user.component';
import { CreateEventComponent } from './create-event/create-event.component';
import { EventPageComponent } from './event-page/event-page.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { UserPanelComponent } from './user-panel/user-panel.component';
import { DonateComponent } from './donate/donate.component';
import { FooterComponent } from './footer/footer.component';

const appRoutes: Routes = [
  {
    path: '',
    component: HomepageComponent
  },
  {
    path: 'event/:id',
    component: EventPageComponent
  },
  {
    path: 'profile',
    component: UserPanelComponent
  },
  {
    path: 'donate/:id',
    component: DonateComponent
  },
  {
    path: 'login',
    component: SignFormComponent,
    children: [
      {
        path: '',
        component: LoginComponent
      }
    ]
  },
  {
    path: 'register',
    component: SignFormComponent,
    children: [
      {
        path: '',
        component: CreateUserComponent
      }
    ]
  },
  {
    path: 'not-found',
    component: NotFoundComponent
  },
  {
    path: 'create-user',
    component: CreateUserComponent
  },
  {
    path: 'create-event',
    component: CreateEventComponent
  }
];

@NgModule({
  declarations: [
    AppComponent,
    HomepageComponent,
    EventPageComponent,
    HeaderComponent,
    NotFoundComponent,
    UserPanelComponent,
    DonateComponent,
    LoginComponent,
    CreateUserComponent,
    SignFormComponent,
    CreateEventComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
