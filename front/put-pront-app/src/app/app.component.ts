import {Component, ViewChild} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'put-pront-app';

  constructor() {
    window.onbeforeunload = () => {
     localStorage.clear();
   };

   localStorage.setItem('logged', 'false');
  }
}
