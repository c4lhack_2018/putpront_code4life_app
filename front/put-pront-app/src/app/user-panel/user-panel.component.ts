import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service';
import { LoginUtils } from '../login-utils';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-panel',
  templateUrl: './user-panel.component.html',
  styleUrls: ['./user-panel.component.css']
})
export class UserPanelComponent implements OnInit {
  constructor(private service: ApiService, private router: Router) {}

  public events: Array<ShortEvent>;

  ngOnInit() {
    LoginUtils.proceedIfLogged(
      () => {
        this.service.getAllUserEvents(parseInt(localStorage.getItem('id'), 10)).subscribe(res => {
          this.events = <ShortEvent[]>res;
        });
      },
      this.router);
  }
}

interface ShortEvent {
  accountId: number;
  certificate: boolean;
  date: any;
  donationAmount: number;
  donationSum: number;
  goalId: number;
  id: number;
  link: string;
  picUrl: string;
  text: string;
  title: string;
}
