export class Ceremony {
    constructor(
        public title: string,
        public date: Date,
        public text: string,
        public picUrl: string,
        public certificate: boolean,
        public donationSum: number,
        public donationAmmount: number,
        public accountId: number,
        public link: string,
        public goalId: number
    ) {}
}

/*

public string Text { get; set; }
public DateTime? Date { get; set; }
public string PicUrl { get; set; }
public bool? Certificate { get; set; }
public string Title { get; set; }
public int? GoalId { get; set; }
public int? DonationSum { get; set; }
public int? DonationAmount { get; set; }
public int? AccountId { get; set; }
public string Link { get; set; }

*/
