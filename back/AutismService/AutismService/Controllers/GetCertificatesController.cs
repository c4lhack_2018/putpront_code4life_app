﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutismService.Models;
using Microsoft.AspNetCore.Mvc;


// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace AutismService.Controllers
{
    [Route("api/[controller]")]
    public class GetCertificatesController : Controller
    {
        private readonly AutismServiceDBContext ctx;

        public GetCertificatesController(AutismServiceDBContext ctx)
        {
            this.ctx = ctx;
        }

        [HttpGet]
        public IEnumerable<Motives> GetCertificates(int id)
        {
            using(var db = ctx)
            {
                var certyficates = db.Motives;
                return certyficates.ToList();
            }
        }

    }
}
