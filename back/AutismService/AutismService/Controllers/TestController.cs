﻿using System;
using System.Collections.Generic;
using System.Linq;

using AutismService.Models;

using Microsoft.AspNetCore.Mvc;
using AutismService.MailUtility;
using System.Drawing;

using System.Text;
using Microsoft.AspNetCore.Hosting;
using System.Drawing.Drawing2D;
using System.Drawing.Text;



// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace AutismService.Controllers
{
    [Route("api/[controller]")]
    public class TestController : Controller
    {
        private readonly IEmailService emailService;
        private readonly AutismServiceDBContext ctx;
        private readonly IHostingEnvironment env;

        public TestController(AutismServiceDBContext ctx, IEmailService emailService, IHostingEnvironment env)
        {
            this.ctx = ctx;
            this.emailService = emailService;
            this.env = env;
        }

        [HttpGet("AllAccounts")]
        public IEnumerable<Accounts> Get(int id)
        {
            return ctx.Accounts.ToList();
        }

        [HttpGet("AllEvents")]
        public IEnumerable<Events> Get2(int id)
        {
            return ctx.Events.ToList();
        }
        [HttpGet("AllDonators")]
        public IEnumerable<Donators> Get3(int id)
        {
            return ctx.Donators.ToList();
        }

        [HttpGet("AllMotives")]
        public IEnumerable<Motives> Get4(int id)
        {
            return ctx.Motives.ToList();
        }
        [HttpGet("AllGoals")]
        public IEnumerable<Goals> Get5(int id)
        {
            return ctx.Goals.ToList();
        }

        [HttpGet("{s}")]
        public string Get(string s)
        {
            string result = "Success!?";
            try
            {
                EmailMessage mail = new EmailMessage();
                EmailAddress receiver = new EmailAddress
                {
                    //Address = "jacgamespam@gmail.com",
                    Address = "rak.wojtek@wp.pl",
                    Name = "Jacek Majtas"
                };
                mail.ToAddresses.Add(receiver);
                mail.Subject = "Test wysyłki 3";
                mail.Content = s;
                emailService.Send(mail);
            }
            catch (Exception e)
            {
                result = e.StackTrace;
            }
            return result;
        }

        [HttpGet("SaveImage")]
        public string SaveImageCert1()
        {
            //hardocded:
            var male = true;
            var name = "Jacek";
            var surname = "Gulij";
            var titleTransformed = "Mariusza Pudzianowskiego";
            var title = "Ślub Marka i Kasi";


            var filePath = env.WebRootPath;
            Bitmap bitmap = null;

            // Create from a stream so we don't keep a lock on the file.
            using (var stream = System.IO.File.OpenRead(filePath + "/" + "certyfikat_bez_tekstu.png"))
            {
                bitmap = (Bitmap)Bitmap.FromStream(stream);
            }

            using (bitmap)
            using (var graphics = Graphics.FromImage(bitmap))
            {
                // Do what you want using the Graphics object here.
                var brush = new SolidBrush(ColorTranslator.FromHtml("#214869"));
                StringFormat format = new StringFormat()
                {
                    Alignment = StringAlignment.Center,
                    LineAlignment = StringAlignment.Center
                };
                graphics.SmoothingMode = SmoothingMode.AntiAlias;

                // The interpolation mode determines how intermediate values between two endpoints are calculated.
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;

                // Use this property to specify either higher quality, slower rendering, or lower quality, faster rendering of the contents of this Graphics object.
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

                // This one is important
                graphics.TextRenderingHint = TextRenderingHint.AntiAliasGridFit;


                // IMIE NAZWISKO
                var font = new Font("Gill Sans MT", 90, FontStyle.Regular);
                RectangleF rect = new RectangleF(0, 1350, bitmap.Width, 200);

                graphics.DrawString(name + " " + surname, font, brush, rect, format);

                // Otrzyał otrzymała
                font = new Font("Gill Sans MT", 60, FontStyle.Regular);
                rect = new RectangleF(0, 1500, bitmap.Width, 100);
                graphics.DrawString(male ? "otrzymał" : "otrzymała", font, brush, rect, format);

                // Opoprzez udzielenie 
                rect.Y = 2200;
                graphics.DrawString("poprzez udzielenie pomocy na rzecz", font, brush, rect, format);

                // Mariusza Pudzianowskiego 
                rect.Y = 2550;
                graphics.DrawString(titleTransformed, font, brush, rect, format);


                // Mariusza Pudzianowskiego 

                rect.Y = 2850;
                graphics.DrawString(title, font, brush, rect, format);



                // Important part!
                graphics.Flush();
                bitmap.Save(filePath + "/" + "certyfikat_z_tekstem.png");
            }
            return "succes;";
        }

        //[HttpGet("SaveImage")]
        //public string SaveImageCert1()
        //{
        //    //hardocded:
        //    var male = true;
        //    var name = "Jacek";
        //    var surname = "Gulij";
        //    var titleTransformed = "Mariusza Pudzianowskiego";
        //    var title = "Ślub Marka i Kasi";


        //    var filePath = env.WebRootPath;
        //    Bitmap bitmap = null;

        //    // Create from a stream so we don't keep a lock on the file.
        //    using (var stream = System.IO.File.OpenRead(filePath + "/" + "certyfikat_bez_tekstu.png"))
        //    {
        //        bitmap = (Bitmap)Bitmap.FromStream(stream);
        //    }

        //    using (bitmap)
        //    using (var graphics = Graphics.FromImage(bitmap))
        //    {
        //        // Do what you want using the Graphics object here.
        //        var brush = new SolidBrush(ColorTranslator.FromHtml("#214869"));


        //        // IMIE NAZWISKO
        //        var font = new Font("Gill Sans MT", 90, FontStyle.Regular);
        //        graphics.DrawString(name + surname, font, brush, 500, 1350);

        //        // Otrzyał otrzymała
        //        var get = male ?  "otrzymał" : "otrzymała";
        //        font = new Font("Gill Sans MT", 60, FontStyle.Regular);
        //        graphics.DrawString(get, font, brush, 1000, 1500) ;

        //        // Opoprzez udzielenie 
        //        //font = new Font("Gill Sans MT", 60, FontStyle.Regular);
        //        //graphics.DrawString("poprzez udzielenie pomocy na rzecz", font, brush, 420, 1800);

        //        // Mariusza Pudzianowskiego 
        //        font = new Font("Gill Sans MT", 60, FontStyle.Regular);
        //        graphics.DrawString(titleTransformed, font, brush, 750, 1850);


        //        // Mariusza Pudzianowskiego 
        //        font = new Font("Gill Sans MT", 60, FontStyle.Regular);
        //        graphics.DrawString(title, font, brush, 650, 2150);



        //        // Important part!
        //        bitmap.Save(filePath + "/" + "certyfikat_z_tekstu.png");
        //    }
        //    return "succes;";
        //}


        //private string GetMail(string s)
        //{
        //    string result = "Success!?";
        //    try
        //    {
        //        EmailMessage mail = new EmailMessage();
        //        EmailAddress receiver = new EmailAddress
        //        {
        //            //Address = "blazej.krzyzanek@student.put.poznan.pl",
        //            //Address = "rak.wojtek@wp.pl",
        //            Address = "jacgamespam@gmail.com",
        //            Name = "Jacek Majtas"
        //        };
        //        mail.ToAddresses.Add(receiver);
        //        mail.Subject = "Test wysyłki 3";
        //        mail.Content = s;
        //        mail.AttachementLocation = hostingEnvironment.WebRootPath + "/" + ctx.Motives.First().Id + ".txt";
        //        emailService.Send(mail);
        //    }
        //    catch (Exception e)
        //    {
        //        result = e.StackTrace;
        //    }
        //    return result;
        //}



    }
}
