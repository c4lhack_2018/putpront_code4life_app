﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutismService.Models;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace AutismService.Controllers
{
    [Route("api/[controller]")]
    public class GetAllEventsController : Controller
    {
        private readonly AutismServiceDBContext ctx;

        public GetAllEventsController(AutismServiceDBContext ctx)
        {
            this.ctx = ctx;
        }
        [HttpGet("{id}")]
        public IEnumerable<Events> GetAllEvents(int id)
        {
            using (var db = ctx)
            {
                var events = db.Events.Where(x => x.AccountId == id);
                return events.ToList();
            }
        }


    }
}
