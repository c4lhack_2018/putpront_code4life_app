﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutismService.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using AutismService.Models.Results;

namespace AutismService.Controllers
{
    [Route("api/Authentication")]
    public class AuthenticationController : Controller
    {
        IMemoryCache cache;

        public AuthenticationController(IMemoryCache cache)
        {
            this.cache = cache;
        }

        [HttpPost]
        public bool GetAuthentication([FromBody] Authentication account)
        {
            cache.TryGetValue(account.Email, out string t);
            return t == account.Token;
        }
    }
}