﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;

namespace AutismService.Models.Results
{
    // You may need to install the Microsoft.AspNetCore.Http.Abstractions package into your project
    public class LoginResult
    {
        public bool LoginSuccesful { get; set; }
        public int AccountId { get; set; }
        public string Email { get; set; }
        public string Login { get; set; }
        public string Token { get; set; }
    }
}
