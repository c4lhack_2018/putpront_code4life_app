﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;

namespace AutismService.Models.Results
{
    public class DonatorsResult : Donators
    {
        public int? Amount { get; set; }
        public int? MotivesId { get; set; }
        public bool? Male { get; set; }
        public string Link { get; set; }
    }
}
