﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;

namespace AutismService.Models.Results
{
    public class EventResult : Events
    {
        public string GoalPicUrl { get; set; }
        public string GoalTitle { get; set; }
        public string GoalDecription { get; set; }
    }
}
