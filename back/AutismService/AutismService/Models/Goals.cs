﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;

namespace AutismService.Models
{
    public partial class Goals
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string GoalPicUrl { get; set; }
        public string Description { get; set; }
        public string TitleTransformed { get; set; }

    }

}
