﻿using System;
using System.Collections.Generic;

namespace AutismService.Models
{
    public partial class Accounts
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
    }
}
