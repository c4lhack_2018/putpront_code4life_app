﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;

namespace AutismService.Models.UploadModels
{
    public class AppIdentitySettings
    {
        public string CouldName { get; set; }
        public string ApiKey { get; set; }
        public string SecretApiKey { get; set; }
    }
}
