﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;

namespace AutismService.Models.UploadModels
{
    public class Image
    {
        public string Caption;
        public string Url;
        public string Format;
        public string PublicId;
    }
}
