﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CloudinaryDotNet.Actions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;

namespace AutismService.Models.UploadModels
{
    class ImageUploadParamsExt : ImageUploadParams
    {
        public string Caption;
    }
}
