﻿using System;
using System.Collections.Generic;

namespace AutismService.Models
{
    public partial class Events
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public DateTime? Date { get; set; }
        public string PicUrl { get; set; }
        public bool? Certificate { get; set; }
        public string Title { get; set; }
        public int? GoalId { get; set; }
        public int? DonationSum { get; set; }
        public int? DonationAmount { get; set; }
        public int? AccountId { get; set; }
        public string Link { get; set; }
    }
}
